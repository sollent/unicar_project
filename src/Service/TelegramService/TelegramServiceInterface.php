<?php

namespace App\Service\TelegramService;

/**
 * Interface TelegramServiceInterface
 */
interface TelegramServiceInterface
{
    public const TELEGRAM_STATUS_MESSAGE_LABEL = 'ИНФОРМАЦИЯ О СТАТУСЕ ОПЕРАЦИИ';

    /**
     * @param string $message
     *
     * @return mixed
     *
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function sendMessage(string $message);
}
