<?php

namespace App\Service\TelegramService;

use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;

/**
 * Class TelegramService
 */
class TelegramService implements TelegramServiceInterface
{
    private Telegram $telegram;

    private int $chatId;

    /**
     * TelegramService constructor.
     *
     * @param string $telegramApiKey
     * @param string $telegramBotUsername
     * @param int $telegramChatId
     *
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function __construct(string $telegramApiKey, string $telegramBotUsername, int $telegramChatId)
    {
        $this->telegram = new Telegram($telegramApiKey, $telegramBotUsername);
        $this->chatId = $telegramChatId;
        Request::initialize($this->telegram);
    }

    /**
     * {@inheritDoc}
     */
    public function sendMessage(string $message)
    {
        $result = Request::sendMessage([
            'chat_id' => -1001496979677,
            'text' => $message,
        ]);
    }
}
