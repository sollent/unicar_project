<?php

namespace App\AMQP\RabbitMQ\Consumer;

use App\Entity\Vehicle\CarPost;
use App\Entity\Vehicle\VehiclePhoto;
use App\Service\TelegramService\TelegramServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VehiclePostPhotosConsumer
 */
class VehiclePostPhotosConsumer implements ConsumerInterface
{
    private EntityManagerInterface $entityManager;

    private ContainerInterface $container;

    /**
     * VehiclePostPhotosConsumer constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param ContainerInterface $container
     */
    public function __construct(EntityManagerInterface $entityManager, ContainerInterface $container)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function execute(AMQPMessage $msg)
    {
        echo PHP_EOL . "Начало выгрузки и обработки фотографий объявлений" . PHP_EOL;

        $carPosts = $this->entityManager->getRepository(CarPost::class)->findBy([
            'previewPhoto' => null
        ]);

        foreach ($carPosts as $carPost) {
            if ($carPost->getPreviewPhotoUrl()) {
                $file = file_get_contents($carPost->getPreviewPhotoUrl());
                $relativePath = '/downloads/' . uniqid() . '.jpg';
                $path = \getcwd() . '/public' . $relativePath;
                file_put_contents($path, $file);

                $vehiclePhoto = new VehiclePhoto();
                $vehiclePhoto
                    ->setPreview(true)
                    ->setPath($relativePath);
                $carPost->setPreviewPhoto($vehiclePhoto);

                $this->entityManager->persist($vehiclePhoto);
            }
        }

        $this->entityManager->flush();

        $this
            ->container
            ->get('old_sound_rabbit_mq.telegram_vehicle_post_status_message_producer')
            ->publish(
                \sprintf(
                    "%s\n\nНазвание задачи: %s\nКол-во обработаных объявлений: %d\nВыполнено для объявлений (ids): %s\nСтатус: %s",
                    TelegramServiceInterface::TELEGRAM_STATUS_MESSAGE_LABEL,
                    'Выгрузка и сохранение заглавных изображений объявлений',
                    \count($carPosts),
                    implode(
                        ', ',
                        \array_map(function (CarPost $carPost) {
                            return $carPost->getId();
                        }, $carPosts)
                    ),
                    'УСПЕШНО'
                )
            );

        echo "Выгрузка и обработка успешно завершена" . PHP_EOL;
    }
}
