<?php

namespace App\AMQP\RabbitMQ\Consumer\Telegram;

use App\Service\TelegramService\TelegramServiceInterface;
use Longman\TelegramBot\Exception\TelegramException;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Class TelegramSendMessageConsumer
 */
class TelegramSendMessageConsumer implements ConsumerInterface
{
    private TelegramServiceInterface $telegramService;

    /**
     * TelegramSendMessageConsumer constructor.
     *
     * @param TelegramServiceInterface $telegramService
     */
    public function __construct(TelegramServiceInterface $telegramService)
    {
        $this->telegramService = $telegramService;
    }

    /**
     * {@inheritDoc}
     */
    public function execute(AMQPMessage $msg)
    {
        echo PHP_EOL . "Отправка сообщение в телеграмм" . PHP_EOL;

        try {
            $this->telegramService->sendMessage($msg->getBody());
        } catch (TelegramException $e) {
        }



        echo "Отправка сообщения завершена" . PHP_EOL;
    }
}
