<?php

namespace App\AMQP\RabbitMQ\Consumer\Telegram;

use App\Repository\VehiclePost\VehiclePostRepositoryInterface;
use App\Service\TelegramService\TelegramServiceInterface;
use Longman\TelegramBot\Exception\TelegramException;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Class TelegramVehiclePostSendMessageConsumer
 */
class TelegramVehiclePostSendMessageConsumer implements ConsumerInterface
{
    private TelegramServiceInterface $telegramService;

    private VehiclePostRepositoryInterface $vehiclePostRepository;

    /**
     * TelegramSendMessageConsumer constructor.
     *
     * @param TelegramServiceInterface $telegramService
     * @param VehiclePostRepositoryInterface $vehiclePostRepository
     */
    public function __construct(
        TelegramServiceInterface $telegramService,
        VehiclePostRepositoryInterface $vehiclePostRepository
    )
    {
        $this->telegramService = $telegramService;
        $this->vehiclePostRepository = $vehiclePostRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function execute(AMQPMessage $msg)
    {
        echo PHP_EOL . "Отправка сообщение (vehicle-post) в телеграмм" . PHP_EOL;

        try {
            $this->telegramService->sendMessage($msg->getBody());
            // Send intermediate status for vehicle posts
            $this->telegramService->sendMessage(
                \sprintf(
                    "%s\nОбщее кол-во объявлений в базе данных: %d",
                    "--- ПРОМЕЖУТОЧНЫЙ ОТЧЕТ ---",
                    $this->vehiclePostRepository->postsCount()
                )
            );
        } catch (TelegramException $e) {
        }



        echo "Отправка сообщения (vehicle-post) завершена" . PHP_EOL;
    }
}
