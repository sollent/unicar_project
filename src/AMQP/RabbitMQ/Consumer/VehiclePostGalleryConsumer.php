<?php

namespace App\AMQP\RabbitMQ\Consumer;

use App\Entity\Vehicle\CarPost;
use App\Entity\Vehicle\VehiclePhoto;
use App\Repository\VehiclePost\VehiclePostRepositoryInterface;
use App\Service\TelegramService\TelegramServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VehiclePostGalleryConsumer
 */
class VehiclePostGalleryConsumer implements ConsumerInterface
{
    private EntityManagerInterface $entityManager;

    private ContainerInterface $container;

    private VehiclePostRepositoryInterface $vehiclePostRepository;

    /**
     * VehiclePostGalleryConsumer constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param ContainerInterface $container
     * @param VehiclePostRepositoryInterface $vehiclePostRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ContainerInterface $container,
        VehiclePostRepositoryInterface $vehiclePostRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
        $this->vehiclePostRepository = $vehiclePostRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function execute(AMQPMessage $msg)
    {
        echo PHP_EOL . "Начало выгрузки и обработки галереи объявлений" . PHP_EOL;

        /** @var CarPost[] $carPosts */
        $carPosts = $this->vehiclePostRepository->findPostWithoutGallery();

        foreach ($carPosts as $carPost) {
            foreach ($carPost->getPhotosUrl() as $photoUrl) {
                $file = file_get_contents($photoUrl);
                $relativePath = '/downloads/' . uniqid() . '.jpg';
                $path = \getcwd() . '/public' . $relativePath;
                file_put_contents($path, $file);

                $vehiclePhoto = new VehiclePhoto();
                $vehiclePhoto
                    ->setPreview(true)
                    ->setPath($relativePath);

                $carPost->addPhoto($vehiclePhoto);

                $this->entityManager->persist($vehiclePhoto);
            }
        }

        $this->entityManager->flush();

        $this
            ->container
            ->get('old_sound_rabbit_mq.telegram_vehicle_post_status_message_producer')
            ->publish(
                \sprintf(
                    "%s\n\nНазвание задачи: %s\nКол-во обработаных объявлений: %d\nВыполнено для объявлений (ids): %s\nСтатус: %s",
                    TelegramServiceInterface::TELEGRAM_STATUS_MESSAGE_LABEL,
                    'Выгрузка и сохранение галереи объявлений',
                    \count($carPosts),
                    implode(
                        ', ',
                        \array_map(function (CarPost $carPost) {
                            return $carPost->getId();
                        }, $carPosts)
                    ),
                    'УСПЕШНО'
                )
            );

        echo "Выгрузка и обработка успешно завершена" . PHP_EOL;
    }
}
