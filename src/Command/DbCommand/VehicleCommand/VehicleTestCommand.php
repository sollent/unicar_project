<?php


namespace App\Command\DbCommand\VehicleCommand;


use App\Entity\Common\Currency;
use App\Entity\Common\Price;
use App\Entity\Vehicle\CarPost;
use App\Entity\Vehicle\VehicleGeneration;
use App\Entity\Vehicle\VehicleMark;
use App\Entity\Vehicle\VehicleModel;
use App\Entity\Vehicle\VehiclePhoto;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

class VehicleTestCommand extends Command
{
    protected static $defaultName = 'vehicle-test';

    private ContainerInterface $container;

    private EntityManagerInterface $entityManager;

    /**
     * LoadVehicleMarksCommand constructor.
     *
     * {@inheritDoc}
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        ContainerInterface $container,
        EntityManagerInterface $entityManager
    )
    {
        parent::__construct();

        $this->container = $container;
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritDoc}
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $outputStyle = new SymfonyStyle($input, $output);

        $carPosts = $this->entityManager->getRepository(CarPost::class)->findBy([
            'previewPhoto' => null
        ]);

        foreach ($carPosts as $carPost) {
            if ($carPost->getPreviewPhotoUrl()) {
                $file = file_get_contents($carPost->getPreviewPhotoUrl());
                $relativePath = '/downloads/' . uniqid() . '.jpg';
                $path = \getcwd() . '/public' . $relativePath;
                file_put_contents($path, $file);

                $vehiclePhoto = new VehiclePhoto();
                $vehiclePhoto
                    ->setPreview(true)
                    ->setPath($relativePath);
                $carPost->setPreviewPhoto($vehiclePhoto);

                $this->entityManager->persist($vehiclePhoto);
            }
        }

        $this->entityManager->flush();

        $outputStyle->success("Success test");

        return 0;
    }
}
