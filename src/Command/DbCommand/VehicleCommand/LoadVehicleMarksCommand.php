<?php

namespace App\Command\DbCommand\VehicleCommand;

use App\Entity\Vehicle\VehicleMark;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class LoadVehicleMarksCommand
 *
 * Using av.by api for loading marks
 */
class LoadVehicleMarksCommand extends Command
{
    protected static $defaultName = 'vehicle:load-marks';

    private EntityManagerInterface $entityManager;

    /**
     * LoadVehicleMarksCommand constructor.
     *
     * {@inheritDoc}
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritDoc}
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $outputStyle = new SymfonyStyle($input, $output);

        $client = new Client([
            'headers' => [ 'Content-Type' => 'application/json' ]
        ]);

        $response = $client->request('post', 'https://api.av.by/offer-types/cars/filters/main/update', [
            RequestOptions::BODY => \json_encode(
                [
                    'properties' => [
                        [
                            'modified' => true,
                            'name' => 'brands'
                        ]
                    ]
                ]
            )
        ]);

        $data = json_decode($response->getBody()->getContents(), true);
        $marksData = $data['properties'][0]['value'][0][0]['options'];

        $existMarks = $this->entityManager->getRepository(VehicleMark::class)->findAll();

        if (\count($existMarks) > 0) {
            $outputStyle->warning("Vehicle marks already exist in database");
            return 0;
        }

        foreach ($marksData as $markData) {
            $mark = new VehicleMark();
            $mark
                ->setTitle($markData['label'])
                ->setAvbyId($markData['id']);

            $this->entityManager->persist($mark);
        }

        $this->entityManager->flush();

        $outputStyle->success("Success marks saving");

        return 0;
    }
}
