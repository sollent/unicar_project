<?php

namespace App\Command\DbCommand\VehicleCommand;

use App\Entity\Vehicle\VehicleGeneration;
use App\Entity\Vehicle\VehicleModel;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class LoadVehicleGenerationsCommand
 */
class LoadVehicleGenerationsCommand extends Command
{
    protected static $defaultName = 'vehicle:load-generations';

    private EntityManagerInterface $entityManager;

    /**
     * LoadVehicleGenerationsCommand constructor.
     *
     * {@inheritDoc}
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritDoc}
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $outputStyle = new SymfonyStyle($input, $output);

        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);

        /** @var VehicleModel[] $models */
        $models = $this->entityManager->getRepository(VehicleModel::class)->findAll();

        if (\count($models) === 0) {
            $outputStyle->error("There are no vehicle models in database for");
            return 0;
        }

        $i = 0;
        foreach ($models as $model) {
            $i++;
            $response = $client->request('post', 'https://api.av.by/offer-types/cars/filters/main/update', [
                RequestOptions::BODY => \json_encode(
                    [
                        'properties' => [
                            [
                                'modified' => true,
                                'name' => 'brands',
                                'value' => [
                                    [
                                        [
                                            'name' => 'brand',
                                            'value' => $model->getMark()->getAvbyId()
                                        ],
                                        [
                                            'name' => 'model',
                                            'value' => $model->getAvbyId(),
                                            'modified' => true
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                )
            ]);

            $data = json_decode($response->getBody()->getContents(), true);
            $generationsData = $data['properties'][0]['value'][0][2]['options'];

            foreach ($generationsData as $generationData) {
                $generation = new VehicleGeneration();
                $generation
                    ->setModel($model)
                    ->setTitle($generationData['label'])
                    ->setFromDate(new \DateTime($generationData['metadata']['yearFrom']))
                    ->setToDate(new \DateTime($generationData['metadata']['yearTo'] ?? 'now'))
                    ->setGenerationImageAvby($generationData['metadata']['file']['url']);

                $this->entityManager->persist($generation);
            }

            $outputStyle->text(\sprintf('---------- %s ----------', round(($i / \count($models)) * 100) . '%'));
        }

        $this->entityManager->flush();

        $outputStyle->success("Success generations saving");

        return 0;
    }
}
