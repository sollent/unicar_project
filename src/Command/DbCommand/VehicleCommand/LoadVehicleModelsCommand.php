<?php

namespace App\Command\DbCommand\VehicleCommand;

use App\Entity\Vehicle\VehicleMark;
use App\Entity\Vehicle\VehicleModel;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class LoadVehicleModelsCommand
 */
class LoadVehicleModelsCommand extends Command
{
    protected static $defaultName = 'vehicle:load-models';

    private EntityManagerInterface $entityManager;

    /**
     * LoadVehicleModelsCommand constructor.
     *
     * {@inheritDoc}
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritDoc}
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $outputStyle = new SymfonyStyle($input, $output);

        $client = new Client([
            'headers' => [ 'Content-Type' => 'application/json' ]
        ]);

        /** @var VehicleMark[] $marks */
        $marks = $this->entityManager->getRepository(VehicleMark::class)->findAll();

        if (\count($marks) === 0) {
            $outputStyle->error("There are no vehicle marks in database for");
            return 0;
        }

        $i = 0;
        foreach ($marks as $mark) {
            $i++;
            $response = $client->request('post', 'https://api.av.by/offer-types/cars/filters/main/update', [
                RequestOptions::BODY => \json_encode(
                    [
                        'properties' => [
                            [
                                'modified' => true,
                                'name' => 'brands',
                                'value' => [
                                    [
                                        [
                                            'name' => 'brand',
                                            'value' => $mark->getAvbyId(),
                                            'modified' => true
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                )
            ]);

            $data = json_decode($response->getBody()->getContents(), true);
            $modelsData = $data['properties'][0]['value'][0][1]['options'];

            foreach ($modelsData as $modelData) {
                $model = new VehicleModel();
                $model
                    ->setMark($mark)
                    ->setTitle($modelData['label'])
                    ->setAvbyId($modelData['id']);

                $this->entityManager->persist($model);
            }

            $outputStyle->text(\sprintf('---------- %s ----------', round(($i / \count($marks)) * 100) . '%'));
        }

        $this->entityManager->flush();

        $outputStyle->success("Success models saving");

        return 0;
    }
}
