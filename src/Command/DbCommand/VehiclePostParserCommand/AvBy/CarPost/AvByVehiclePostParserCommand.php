<?php

namespace App\Command\DbCommand\VehiclePostParserCommand\AvBy\CarPost;

use App\Entity\Common\Currency;
use App\Entity\Common\Price;
use App\Entity\Vehicle\CarPost;
use App\Entity\Vehicle\VehicleGeneration;
use App\Entity\Vehicle\VehicleMark;
use App\Entity\Vehicle\VehicleModel;
use App\Service\TelegramService\TelegramServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AvByVehiclePostParserCommand
 */
class AvByVehiclePostParserCommand extends Command
{
    protected static $defaultName = 'vehicle-post-parser:av-by:car-post';

    private ContainerInterface $container;

    private EntityManagerInterface $entityManager;

    /**
     * LoadVehicleMarksCommand constructor.
     *
     * {@inheritDoc}
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        ContainerInterface $container,
        EntityManagerInterface $entityManager
    )
    {
        parent::__construct();

        $this->container = $container;
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritDoc}
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $outputStyle = new SymfonyStyle($input, $output);

        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);

        $response = $client->request('get', 'https://api.av.by/offer-types/cars/filters/main/init?price_currency=2&sort=4');

        $data = \json_decode($response->getBody()->getContents(), true);

        $carPosts = [];

        $lastLinks = \array_map(function (CarPost $carPost) {
            return $carPost->getResourceUrl();
        }, $this->entityManager->getRepository(CarPost::class)->findBy([], ['createdAt' => 'DESC'], 50));

        $neededAdverts = $this->calculateNeededLinks($data['adverts'], $lastLinks);


        foreach ($neededAdverts as $advert) {
            $carPost = new CarPost();
            foreach ($advert['properties'] as $property) {
                if ($property['name'] === 'brand') {
                    $markName = $property['value'];
                    $existingMark = $this->entityManager->getRepository(VehicleMark::class)->findOneBy([
                        'title' => $markName
                    ]);
                    if ($existingMark) {
                        $carPost->setMark($existingMark);
                    } else {
                        $outputStyle->error('In car post has no mark info');
                        return 0;
                    }
                }
            }
            foreach ($advert['properties'] as $property) {
                // model handling
                if ($property['name'] === 'model') {
                    $modelName = $property['value'];
                    $existingModel = $this->entityManager->getRepository(VehicleModel::class)->findOneBy([
                        'title' => $modelName,
                        'mark' => $existingMark
                    ]);
                    if ($existingMark) {
                        $carPost->setModel($existingModel);
                    }
                }
            }
            foreach ($advert['properties'] as $property) {
                // generation handling
                if ($property['name'] === 'generation') {
                    $modelName = $property['value'];
                    $existingGeneration = $this->entityManager->getRepository(VehicleGeneration::class)->findOneBy([
                        'title' => $modelName,
                        'model' => $existingModel
                    ]);
                    if ($existingGeneration) {
                        $carPost->setGeneration($existingGeneration);
                    }
                }
            }

            if ($advert['price']['usd']) {
                $currency = $this->entityManager->getRepository(Currency::class)->findOneBy([
                    'title' => $advert['price']['usd']['currency']
                ]);
            } else {
                throw new \Exception('Need to handle other currencies situations');
            }

            $carPost
                ->setPrice(new Price($currency, (int)$advert['price'][$currency->getTitle()]['amount']))
                ->setYear((int)$advert['year']);

            foreach ($advert['photos'] as $advertPhoto) {
                if ($advertPhoto['main'] === true) {
                    $carPost->setPreviewPhotoUrl($advertPhoto['medium']['url']);
                }
            }

            $urls = [];
            foreach ($advert['photos'] as $advertPhoto) {
                $urls[] = $advertPhoto['medium']['url'];
            }
            $carPost->setPhotosUrl($urls);
            $carPost->setResourceUrl($advert['publicUrl']);

            $this->entityManager->persist($carPost);

            $carPosts[] = $carPost;
        }

        $this->entityManager->flush();

        $outputStyle->success("Success car posts saving");

        $this
            ->container
            ->get('old_sound_rabbit_mq.telegram_vehicle_post_status_message_producer')
            ->publish(
                \sprintf(
                    "%s\n\nНазвание задачи: %s\nКол-во обработаных объявлений: %d\nВыполнено для объявлений (ids): %s\nСтатус: %s",
                    TelegramServiceInterface::TELEGRAM_STATUS_MESSAGE_LABEL,
                    'Первичая выгрузка свежих объявлений легковых автомобилей',
                    \count($neededAdverts),
                    implode(
                        ', ',
                        \array_map(function (CarPost $carPost) {
                            return $carPost->getId();
                        }, $carPosts)
                    ),
                    'УСПЕШНО'
                )
            );

        $this
            ->container
            ->get('old_sound_rabbit_mq.vehicle_post_photos_producer')
            ->publish('');
        $this
            ->container
            ->get('old_sound_rabbit_mq.vehicle_post_gallery_producer')
            ->publish('');

        return 0;
    }

    /**
     * @param array $actualAdverts
     * @param array $lastLinks
     *
     * @return string[]
     */
    private function calculateNeededLinks(array $actualAdverts, array $lastLinks): array
    {
        for ($i = 0; $i < \count($lastLinks); $i++) {
            for ($j = 0; $j < \count($actualAdverts); $j++) {
                if ($actualAdverts[$j]['publicUrl'] === $lastLinks[$i]) {
                    \array_splice($actualAdverts, $j, 1);
                }
            }
        }

        return $actualAdverts;
    }
}
