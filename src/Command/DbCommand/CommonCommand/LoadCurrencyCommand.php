<?php

namespace App\Command\DbCommand\CommonCommand;

use App\Entity\Common\Currency;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class LoadCurrencyCommand
 */
class LoadCurrencyCommand extends Command
{
    protected static $defaultName = 'common:load-currencies';

    private EntityManagerInterface $entityManager;

    /**
     * @var array
     */
    private array $currencies = [
        'byn' => [
            'sign' => 'BYN',
            'startPosition' => false
        ],
        'eur' => [
            'sign' => '€',
            'startPosition' => true
        ],
        'rub' => [
            'sign' => 'RUB',
            'startPosition' => false
        ],
        'usd' => [
            'sign' => '$',
            'startPosition' => true
        ]
    ];

    /**
     * LoadVehicleGenerationsCommand constructor.
     *
     * {@inheritDoc}
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        foreach ($this->currencies as $key => $currencyData) {
            $currency = new Currency();
            $currency
                ->setTitle($key)
                ->setSign($currencyData['sign'])
                ->setSignStartPosition($currencyData['startPosition']);

            $this->entityManager->persist($currency);
        }

        $this->entityManager->flush();

        $outputStyle = new SymfonyStyle($input, $output);
        $outputStyle->success("Success currencies saving");

        return 0;
    }
}
