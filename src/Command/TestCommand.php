<?php

namespace App\Command;

use App\Entity\Vehicle\CarPost;
use App\Entity\Vehicle\VehiclePhoto;
use App\Repository\VehiclePost\VehiclePostRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TestCommand extends Command
{
    protected static $defaultName = 'test:command';

    private EntityManagerInterface $entityManager;

    private ContainerInterface $container;

    private VehiclePostRepositoryInterface $vehiclePostRepository;

    /**
     * VehiclePostGalleryConsumer constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param ContainerInterface $container
     * @param VehiclePostRepositoryInterface $vehiclePostRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ContainerInterface $container,
        VehiclePostRepositoryInterface $vehiclePostRepository
    )
    {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->container = $container;
        $this->vehiclePostRepository = $vehiclePostRepository;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var CarPost[] $carPosts */
        $carPosts = $this->vehiclePostRepository->findPostWithoutGallery();

        $carPosts = $this->entityManager->getRepository(CarPost::class)->findBy([], [], 2);

        foreach ($carPosts as $carPost) {
            foreach ($carPost->getPhotosUrl() as $photoUrl) {
                $file = file_get_contents($photoUrl);
                $relativePath = '/downloads/' . uniqid() . '.jpg';
                $path = \getcwd() . '/public' . $relativePath;
                file_put_contents($path, $file);

                $vehiclePhoto = new VehiclePhoto();
                $vehiclePhoto
                    ->setPreview(true)
                    ->setPath($relativePath);

                $carPost->addPhoto($vehiclePhoto);

                $this->entityManager->persist($vehiclePhoto);
            }
        }

        $this->entityManager->flush();
    }
}
