<?php

namespace App\Controller;

use App\Repository\VehiclePost\VehiclePostRepositoryInterface;
use App\Service\TelegramService\TelegramService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route(
     *     "/"
     * )
     *
     * @param TelegramService $telegramService
     *
     * @param VehiclePostRepositoryInterface $vehiclePostRepository
     * @return Response
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function index(TelegramService $telegramService, VehiclePostRepositoryInterface $vehiclePostRepository): Response
    {
        $vehiclePostRepository->findPostWithoutGallery();
        $telegramService->sendMessage('asdasd asjd asjd asdas');
    }
}
