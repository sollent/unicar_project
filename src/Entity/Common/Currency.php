<?php

namespace App\Entity\Common;

use App\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Currency
 *
 * @ORM\Entity
 */
class Currency extends AbstractEntity
{
    /**
     * @ORM\Column(type="string")
     */
    private string $title;

    /**
     * @ORM\Column(type="string")
     */
    private string $sign;

    /**
     * @ORM\Column(type="boolean", options={"default": 1})
     */
    private bool $signStartPosition = true;

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Currency
     */
    public function setTitle(string $title): Currency
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getSign(): string
    {
        return $this->sign;
    }

    /**
     * @param string $sign
     * @return Currency
     */
    public function setSign(string $sign): Currency
    {
        $this->sign = $sign;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSignStartPosition(): bool
    {
        return $this->signStartPosition;
    }

    /**
     * @param bool $signStartPosition
     * @return Currency
     */
    public function setSignStartPosition(bool $signStartPosition): Currency
    {
        $this->signStartPosition = $signStartPosition;
        return $this;
    }
}
