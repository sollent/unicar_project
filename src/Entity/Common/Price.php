<?php

namespace App\Entity\Common;

use App\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class VehiclePrice
 *
 * @ORM\Entity
 */
class Price extends AbstractEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Common\Currency")
     */
    private Currency $currency;

    /**
     * @ORM\Column(type="integer")
     */
    private int $amount;

    /**
     * {@inheritDoc}
     */
    public function __construct(Currency $currency, int $amount)
    {
        parent::__construct();

        $this->currency = $currency;
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return \sprintf(
            '%s %s',
            $this->currency->isSignStartPosition() ? $this->currency->getSign() : $this->amount,
        $this->currency->isSignStartPosition() ? $this->amount : $this->currency->getSign()
        );
    }

    /**
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     * @param Currency $currency
     * @return Price
     */
    public function setCurrency(Currency $currency): Price
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return Price
     */
    public function setAmount(int $amount): Price
    {
        $this->amount = $amount;
        return $this;
    }
}
