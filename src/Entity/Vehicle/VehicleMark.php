<?php

namespace App\Entity\Vehicle;

use App\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class VehicleMark
 *
 * @ORM\Entity
 */
class VehicleMark extends AbstractEntity
{
    /**
     * @ORM\Column(type="string")
     */
    private string $title;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $avbyId = null;

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return VehicleMark
     */
    public function setTitle(string $title): VehicleMark
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getAvbyId(): ?int
    {
        return $this->avbyId;
    }

    /**
     * @param int|null $avbyId
     * @return VehicleMark
     */
    public function setAvbyId(?int $avbyId): VehicleMark
    {
        $this->avbyId = $avbyId;
        return $this;
    }
}
