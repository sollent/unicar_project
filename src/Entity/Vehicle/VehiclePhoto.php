<?php

namespace App\Entity\Vehicle;

use App\Entity\AbstractEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class VehiclePhoto
 *
 * @ORM\Entity
 */
class VehiclePhoto extends AbstractEntity
{
    /**
     * @ORM\Column(type="string")
     */
    private string $path;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $preview = false;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Vehicle\VehiclePost")
     */
    private ?VehiclePost $post = null;

    /**
     * @var Collection|VehiclePost[]
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Vehicle\VehiclePost", inversedBy="photos")
     */
    private $posts;

    /**
     * VehiclePhoto constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->posts = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return VehiclePhoto
     */
    public function setPath(string $path): VehiclePhoto
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPreview(): bool
    {
        return $this->preview;
    }

    /**
     * @param bool $preview
     * @return VehiclePhoto
     */
    public function setPreview(bool $preview): VehiclePhoto
    {
        $this->preview = $preview;
        return $this;
    }

    /**
     * @return VehiclePost
     */
    public function getPost(): VehiclePost
    {
        return $this->post;
    }

    /**
     * @param VehiclePost $post
     * @return VehiclePhoto
     */
    public function setPost(VehiclePost $post): VehiclePhoto
    {
        $this->post = $post;
        return $this;
    }

    /**
     * @return VehiclePost[]|Collection[]
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param VehiclePost[]|Collection[] $posts
     * @return VehiclePhoto
     */
    public function setPosts($posts)
    {
        $this->posts = $posts;
        return $this;
    }

    /**
     * @param VehiclePost $post
     *
     * @return void
     */
    public function addPost(VehiclePost $post): void
    {
        if (!$this->posts->contains($post)) {
            $this->posts->add($post);
        }
    }

    /**
     * @param VehiclePost $post
     *
     * @return void
     */
    public function removePost(VehiclePost $post): void
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
        }
    }
}
