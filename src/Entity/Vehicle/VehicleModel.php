<?php

namespace App\Entity\Vehicle;

use App\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class VehicleModel
 *
 * @ORM\Entity
 */
class VehicleModel extends AbstractEntity
{
    /**
     * @ORM\Column(type="string")
     */
    private string $title;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $avbyId = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Vehicle\VehicleMark")
     */
    private VehicleMark $mark;

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return VehicleModel
     */
    public function setTitle(string $title): VehicleModel
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return VehicleMark
     */
    public function getMark(): VehicleMark
    {
        return $this->mark;
    }

    /**
     * @param VehicleMark $mark
     * @return VehicleModel
     */
    public function setMark(VehicleMark $mark): VehicleModel
    {
        $this->mark = $mark;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getAvbyId(): ?int
    {
        return $this->avbyId;
    }

    /**
     * @param int|null $avbyId
     * @return VehicleModel
     */
    public function setAvbyId(?int $avbyId): VehicleModel
    {
        $this->avbyId = $avbyId;
        return $this;
    }
}
