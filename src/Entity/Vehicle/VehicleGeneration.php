<?php

namespace App\Entity\Vehicle;

use App\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class VehicleGeneration
 *
 * @ORM\Entity
 */
class VehicleGeneration extends AbstractEntity
{
    /**
     * @ORM\Column(type="string")
     */
    private string $title;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Vehicle\VehicleModel")
     */
    private VehicleModel $model;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private ?\DateTime $fromDate = null;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private ?\DateTime $toDate = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $generationImageAvby = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $generationImage = null;

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return VehicleGeneration
     */
    public function setTitle(string $title): VehicleGeneration
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getFromDate(): ?\DateTime
    {
        return $this->fromDate;
    }

    /**
     * @param \DateTime|null $fromDate
     * @return VehicleGeneration
     */
    public function setFromDate(?\DateTime $fromDate): VehicleGeneration
    {
        $this->fromDate = $fromDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getToDate(): ?\DateTime
    {
        return $this->toDate;
    }

    /**
     * @param \DateTime|null $toDate
     * @return VehicleGeneration
     */
    public function setToDate(?\DateTime $toDate): VehicleGeneration
    {
        $this->toDate = $toDate;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGenerationImage(): ?string
    {
        return $this->generationImage;
    }

    /**
     * @param string|null $generationImage
     * @return VehicleGeneration
     */
    public function setGenerationImage(?string $generationImage): VehicleGeneration
    {
        $this->generationImage = $generationImage;
        return $this;
    }

    /**
     * @return VehicleModel
     */
    public function getModel(): VehicleModel
    {
        return $this->model;
    }

    /**
     * @param VehicleModel $model
     * @return VehicleGeneration
     */
    public function setModel(VehicleModel $model): VehicleGeneration
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGenerationImageAvby(): ?string
    {
        return $this->generationImageAvby;
    }

    /**
     * @param string|null $generationImageAvby
     * @return VehicleGeneration
     */
    public function setGenerationImageAvby(?string $generationImageAvby): VehicleGeneration
    {
        $this->generationImageAvby = $generationImageAvby;
        return $this;
    }
}
