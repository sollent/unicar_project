<?php

namespace App\Entity\Vehicle;

use App\Entity\AbstractEntity;
use App\Entity\Common\Price;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Vehicle
 *
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="vehicle_post_type", type="string")
 * @ORM\DiscriminatorMap({
 *      "car_post"="App\Entity\Vehicle\CarPost"
 * })
 *
 * @ORM\Entity
 */
abstract class VehiclePost extends AbstractEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Vehicle\VehicleMark", cascade={"persist"})
     */
    protected VehicleMark $mark;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Vehicle\VehicleModel", cascade={"persist"})
     */
    protected ?VehicleModel $model = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Vehicle\VehicleGeneration", cascade={"persist"})
     */
    protected ?VehicleGeneration $generation = null;

    /**
     * @ORM\Column(type="integer")
     */
    protected int $year;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Common\Price", cascade={"persist", "remove"})
     */
    protected Price $price;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected ?string $sellerName = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected ?string $phoneNumber = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $previewPhotoUrl = null;

    /**
     * @ORM\Column(type="array")
     */
    private array $photosUrl = [];

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $resourceUrl = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Vehicle\VehiclePhoto", cascade={"persist"})
     */
    private ?VehiclePhoto $previewPhoto = null;

    /**
     * @var Collection|VehiclePhoto[]
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Vehicle\VehiclePhoto", mappedBy="posts", cascade={"persist", "remove"})
     */
    private $photos;

    /**
     * @return string
     */
    public function __toString(): string
    {
        return \trim(\sprintf('%s %s %s', $this->mark, $this->model, $this->generation));
    }

    /**
     * VehiclePost constructor.
     *
     * {@inheritDoc}
     */
    public function __construct()
    {
        parent::__construct();

        $this->photos = new ArrayCollection();
    }

    /**
     * @return VehicleMark
     */
    public function getMark(): VehicleMark
    {
        return $this->mark;
    }

    /**
     * @param VehicleMark $mark
     * @return VehiclePost
     */
    public function setMark(VehicleMark $mark): VehiclePost
    {
        $this->mark = $mark;
        return $this;
    }

    /**
     * @return VehicleModel|null
     */
    public function getModel(): ?VehicleModel
    {
        return $this->model;
    }

    /**
     * @param VehicleModel|null $model
     * @return VehiclePost
     */
    public function setModel(?VehicleModel $model): VehiclePost
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return VehicleGeneration|null
     */
    public function getGeneration(): ?VehicleGeneration
    {
        return $this->generation;
    }

    /**
     * @param VehicleGeneration|null $generation
     * @return VehiclePost
     */
    public function setGeneration(?VehicleGeneration $generation): VehiclePost
    {
        $this->generation = $generation;
        return $this;
    }

    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }

    /**
     * @param int $year
     * @return VehiclePost
     */
    public function setYear(int $year): VehiclePost
    {
        $this->year = $year;
        return $this;
    }

    /**
     * @return Price
     */
    public function getPrice(): Price
    {
        return $this->price;
    }

    /**
     * @param Price $price
     * @return VehiclePost
     */
    public function setPrice(Price $price): VehiclePost
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSellerName(): ?string
    {
        return $this->sellerName;
    }

    /**
     * @param string|null $sellerName
     * @return VehiclePost
     */
    public function setSellerName(?string $sellerName): VehiclePost
    {
        $this->sellerName = $sellerName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string|null $phoneNumber
     * @return VehiclePost
     */
    public function setPhoneNumber(?string $phoneNumber): VehiclePost
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    /**
     * @return VehiclePhoto|null
     */
    public function getPreviewPhoto(): ?VehiclePhoto
    {
        return $this->previewPhoto;
    }

    /**
     * @param VehiclePhoto|null $previewPhoto
     * @return VehiclePost
     */
    public function setPreviewPhoto(?VehiclePhoto $previewPhoto): VehiclePost
    {
        $this->previewPhoto = $previewPhoto;
        return $this;
    }

    /**
     * @return VehiclePhoto[]|Collection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * @param VehiclePhoto[]|Collection $photos
     * @return VehiclePost
     */
    public function setPhotos($photos)
    {
        $this->photos = $photos;
        return $this;
    }

    /**
     * @param VehiclePhoto $photo
     *
     * @return void
     */
    public function addPhoto(VehiclePhoto $photo): void
    {
        if (!$this->photos->contains($photo)) {
            $this->photos->add($photo);
            $photo->addPost($this);
        }
    }

    /**
     * @param VehiclePhoto $photo
     *
     * @return void
     */
    public function removePhoto(VehiclePhoto $photo): void
    {
        if ($this->photos->contains($photo)) {
            $this->photos->removeElement($photo);
            $photo->removePost($this);
        }
    }

    /**
     * @return string|null
     */
    public function getPreviewPhotoUrl(): ?string
    {
        return $this->previewPhotoUrl;
    }

    /**
     * @param string|null $previewPhotoUrl
     * @return VehiclePost
     */
    public function setPreviewPhotoUrl(?string $previewPhotoUrl): VehiclePost
    {
        $this->previewPhotoUrl = $previewPhotoUrl;
        return $this;
    }

    /**
     * @return array
     */
    public function getPhotosUrl(): array
    {
        return $this->photosUrl;
    }

    /**
     * @param array $photosUrl
     * @return VehiclePost
     */
    public function setPhotosUrl(array $photosUrl): VehiclePost
    {
        $this->photosUrl = $photosUrl;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getResourceUrl(): ?string
    {
        return $this->resourceUrl;
    }

    /**
     * @param string|null $resourceUrl
     * @return VehiclePost
     */
    public function setResourceUrl(?string $resourceUrl): VehiclePost
    {
        $this->resourceUrl = $resourceUrl;
        return $this;
    }
}
