<?php

namespace App\Repository\VehiclePost;

use App\Entity\Vehicle\VehiclePhoto;
use App\Entity\Vehicle\VehiclePost;
use App\Repository\AbstractRepository;
use function Doctrine\ORM\QueryBuilder;

/**
 * Class VehiclePostRepository
 */
class VehiclePostRepository extends AbstractRepository implements VehiclePostRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    protected function getEntityClass(): string
    {
        return VehiclePost::class;
    }

    /**
     * {@inheritDoc}
     */
    public function postsCount(): int
    {
        $queryBuilder = $this->createQueryBuilder('vehicle_post');
        $queryBuilder->select('COUNT(vehicle_post.id)');

        return (int) $queryBuilder->getQuery()->getSingleScalarResult();
    }

    /**
     * {@inheritDoc}
     */
    public function findPostWithoutGallery(): array
    {
        $queryBuilder = $this->createQueryBuilder('vehicle_post');
        $queryBuilder->where('vehicle_post.photos is empty');

        return  $queryBuilder->getQuery()->getResult();
    }
}
