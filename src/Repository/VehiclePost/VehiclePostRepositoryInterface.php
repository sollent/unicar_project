<?php

namespace App\Repository\VehiclePost;

/**
 * Interface VehiclePostRepositoryInterface
 */
interface VehiclePostRepositoryInterface
{
    /**
     * @return int
     */
    public function postsCount(): int;

    /**
     * @return array
     */
    public function findPostWithoutGallery(): array;
}
