<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\Container0AoufXx\srcApp_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/Container0AoufXx/srcApp_KernelDevDebugContainer.php') {
    touch(__DIR__.'/Container0AoufXx.legacy');

    return;
}

if (!\class_exists(srcApp_KernelDevDebugContainer::class, false)) {
    \class_alias(\Container0AoufXx\srcApp_KernelDevDebugContainer::class, srcApp_KernelDevDebugContainer::class, false);
}

return new \Container0AoufXx\srcApp_KernelDevDebugContainer([
    'container.build_hash' => '0AoufXx',
    'container.build_id' => 'ba3eaceb',
    'container.build_time' => 1631530821,
], __DIR__.\DIRECTORY_SEPARATOR.'Container0AoufXx');
